package User;

import java.util.ArrayList;
import java.util.List;

public class Privilage extends Entity {

	private int roleId;
	private int permissionId;
	private List<Role> roles;

	public RolesPermission()
	{
		setRoles(new ArrayList<Role>());
	}
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	public int getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	public void addRole(Role role) {
		roles.add(role);
	}
}