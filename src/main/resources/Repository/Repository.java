package Repository;

import java.util.List;

public interface Repository<TEntity> {

	TEntity withId(int id);
	void add(TEntity entity);
	void delete(TEntity entity);
	void modify(TEntity entity);
	int count();
	
	
}